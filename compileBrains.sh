#!/bin/sh

if [[ $# -eq 0 ]]; then
    java -cp build/libs/sopra2018_ki.jar saarland.cispa.sopra.Main --in src/main/resources/KellR_brains/ --out src/main/resources/brains/
elif [[ $# -eq 2 ]]; then
    java -cp build/libs/sopra2018_ki.jar saarland.cispa.sopra.Main --ki $1 --out $2
else
    echo "usage: compileBrains <KellR brain filename> <outfile>"
fi
