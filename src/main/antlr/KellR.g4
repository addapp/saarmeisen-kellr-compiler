grammar KellR;

/* Word tokens */

kellR : functionDeclaration* brain EOF ;

functionDeclaration : anyws? 'function' anyws IDENTIFIER anyws? block newline anyws?;

brain : 'brain' anyws QUOTE IDENTIFIER QUOTE anyws? block anyws? ;

block : LCURLY anyws? statementBase+ anyws? RCURLY ;

statementBase : anyws? labelDefinition* anyws? statement newline;
statement : (ifStatement
           | whileStatement
           | functionCallStatement
           | returnStatement
//           | forStatement
           | instruction) ;

ifStatement : 'if' anyws? LBRACE anyws? NEGATION? booleanInstructionBase anyws? RBRACE anyws? block (anyws? ELSE anyws? block)? ;
whileStatement : 'while' anyws? LBRACE anyws? NEGATION? booleanInstructionBase anyws? RBRACE anyws? block ;
functionCallStatement : IDENTIFIER anyws? LBRACE RBRACE ;
returnStatement : 'return' ;
//forStatement : 'for' anyws? LBRACE anyws? NUMBER anyws? RBRACE anyws? block ;

instruction : normalInstruction
            | booleanInstruction
            ;

normalInstruction : mark
                  | unmark
                  | set
                  | unset
                  | turn
                  | jump
                  ;

booleanInstruction : booleanInstructionBase anywsNoNewline ELSE anywsNoNewline jumpTarget ;

booleanInstructionBase : (move
                        | breed
                        | test
                        | flip
                        | direction
                        | sense
                        | senseMark
                        | drop
                        | pickup)
                       ;

mark      : 'mark' anywsNoNewline NUMBER ;
unmark    : 'unmark' anywsNoNewline NUMBER ;
set       : 'set' anywsNoNewline NUMBER ;
unset     : 'unset' anywsNoNewline NUMBER ;
turn      : 'turn' anywsNoNewline leftright ;
jump      : 'jump' anywsNoNewline jumpTarget ;

move      : 'move' ;
breed     : 'breed' ;
test      : 'test' anywsNoNewline NUMBER ;
direction : 'direction' anywsNoNewline DIRECTION ;
sense     : 'sense' anywsNoNewline relativeposition anywsNoNewline CONDITION ;
senseMark : 'sense' anywsNoNewline relativeposition anywsNoNewline 'marker' anywsNoNewline NUMBER ;
flip      : 'flip' anywsNoNewline NUMBER ;
drop      : 'drop' ;
pickup    : 'pickup' ;

// NUMBER and NEWGATIVENUMBER are relative addresses
jumpTarget : LABEL /*| NUMBER | NEGATIVENUMBER*/ ;

labelDefinition : LABEL ':' anyws? ;


/* Whitespace */
anywsNoNewline : (SPACE
                | BlockCommentNoNewline
                )+
                ;

anyws : (SPACE
       | BlockCommentNoNewline
       | newline)+
       ;

newline : anywsNoNewline? (NEWLINE
                         | LineComment
                         | BlockCommentWithNewline)
                         ;

NEWLINE : '\n' | '\r' ;
SPACE : ' ' | '\t' ;
BlockCommentNoNewline : '/*' (~[\n\r])*? '*/' ;
BlockCommentWithNewline : '/*' .*? '*/' ;
LineComment : '//' .*? NEWLINE ;

ELSE : 'else' ;
NEGATION : '!' ;
QUOTE : '"' ;
LCURLY : '{' ;
RCURLY : '}' ;
LBRACE : '(' ;
RBRACE : ')' ;
MINUS  : '-' ;

NEGATIVENUMBER   : '-' [0-9]+ ;
NUMBER           : [0-9]+ ;
relativeposition : 'here' | 'ahead' | leftright ;
leftright        : 'left' | 'right' ;
CONDITION        : 'food' | 'friend' | 'foe' | 'rock' | 'home' | 'foehome'
                 | 'foemarker' | 'friendfood' | 'foefood' | 'antlion' ;
DIRECTION        : 'northwest' | 'northeast' | 'east' | 'southeast'
                 | 'southwest' | 'west' ;
IDENTIFIER       : [a-zA-Z_.-][a-zA-Z0-9_.-]+ ;
LABEL            : '@' [a-zA-Z0-9_.-]+ ;
REST             : . ;
