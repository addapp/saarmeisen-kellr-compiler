package saarland.cispa.sopra;

import java.util.HashMap;
import java.util.Map;

public class LabelFactory {
    private final Map<String, Label> labelMap;
    private int nextLabelId;

    public LabelFactory() {
        labelMap = new HashMap<>();
    }

    public Label getLabel(String name) {
        Label label = labelMap.get(name);

        if (label == null) {
            return makeLabel(name);
        } else {
            return label;
        }
    }

    public Label getUniqueLabel () {
        String name = "$" + nextLabelId;
        return makeLabel(name);
    }

    private Label makeLabel(String name) {
        Label newLabel = new Label(nextLabelId, name);
        labelMap.put(name, newLabel);
        nextLabelId++;
        return newLabel;
    }
}
