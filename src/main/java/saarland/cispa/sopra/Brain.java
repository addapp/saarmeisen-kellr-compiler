package saarland.cispa.sopra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import saarland.cispa.sopra.statements.BlockStatement;
import saarland.cispa.sopra.statements.InstructionStatement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Brain {
    private final BlockStatement block;
    private final String name;
    private static final Logger logger = LoggerFactory.getLogger(Brain.class);

    public Brain(String name, BlockStatement block) {
        this.name = name;
        this.block = block;
    }

    public String writeToAcola() {

        // turn everything into a flat list of instructions
        List<InstructionStatement> instructions = block.compileStatement();
        setLabelLines(instructions);
        List<InstructionStatement> finalInstructions = instructions.stream().filter(InstructionStatement::isProperInstruction).collect(Collectors.toList());
        assignJumpAdresses(finalInstructions);
        squashJumpChains(finalInstructions);

        // write the program as a string
        StringBuilder stringBuilder = new StringBuilder(String.format("brain \"%s\" {%n", name));
        for (InstructionStatement instructionStatement : instructions) {
            instructionStatement.writeToAcola(stringBuilder);
        }
        stringBuilder.append("}\n");
        return stringBuilder.toString();
    }

    /**
     * Assigns each label it's line number in the environment
     * @param instructions
     */
    private void setLabelLines(List<InstructionStatement> instructions) {
        // assign labels to numbers
        int line = 0;
        for (InstructionStatement instructionStatement : instructions) {
            line = instructionStatement.setLabelsLine(line);
        }
    }

    /**
     *
     * @param instructions
     */
    private void assignJumpAdresses(List<InstructionStatement> instructions) {
        for (InstructionStatement instructionStatement : instructions) {
            instructionStatement.assignJumpAddress();
        }
    }

    private void squashJumpChains(List<InstructionStatement> instructions) {
        // Go through the instructions and squash consecutive jumps into one
        for (InstructionStatement instruction : instructions) {
            // Check if we are starting a jump chain
            if (instruction.isJumpStart()) {
                List<InstructionStatement> jumpChain = new ArrayList<>();
                jumpChain.add(instruction);

                InstructionStatement current = instruction;
                InstructionStatement next = instructions.get(current.getJumpAddress());

                while (next.isJump() && !next.isSquashed()) {
                    // found a loop
                    if (jumpChain.contains(next)) {
                        logger.warn("Found a loop of jump instructions: {}", jumpChain);
                        break;
                    }

                    jumpChain.add(next);
                    current = next;
                    next = instructions.get(current.getJumpAddress());
                }

                for (InstructionStatement jumpLink : jumpChain) {
                    jumpLink.setJumpAddressAndSquash(current.getJumpAddress());
                }
            }
        }
    }
}
