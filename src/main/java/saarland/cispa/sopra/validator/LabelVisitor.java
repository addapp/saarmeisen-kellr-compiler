package saarland.cispa.sopra.validator;

import saarland.cispa.sopra.Label;
import saarland.cispa.sopra.LabelFactory;
import saarland.cispa.sopra.antlr.KellRBaseVisitor;
import saarland.cispa.sopra.antlr.KellRParser;

public class LabelVisitor extends KellRBaseVisitor<Label> {
    private final LabelFactory labelFactory;

    public LabelVisitor(LabelFactory labelFactory) {
        this.labelFactory = labelFactory;
    }

    @Override
    public Label visitLabelDefinition(KellRParser.LabelDefinitionContext ctx) {
        return labelFactory.getLabel(ctx.LABEL().getText());
    }
}
