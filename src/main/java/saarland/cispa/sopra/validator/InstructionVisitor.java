package saarland.cispa.sopra.validator;

import saarland.cispa.sopra.LabelFactory;
import saarland.cispa.sopra.antlr.KellRBaseVisitor;
import saarland.cispa.sopra.antlr.KellRParser;
import saarland.cispa.sopra.instructions.AcolaInstruction;


public class InstructionVisitor extends KellRBaseVisitor<AcolaInstruction> {
    private final NormalInstructionVisitor normalInstructionVisitor;
    private final BooleanInstructionVisitor booleanInstructionVisitor;

    public InstructionVisitor(LabelFactory labelFactory) {
        normalInstructionVisitor = new NormalInstructionVisitor(labelFactory);
        booleanInstructionVisitor = new BooleanInstructionVisitor(labelFactory);
    }

    @Override
    public AcolaInstruction visitNormalInstruction(KellRParser.NormalInstructionContext ctx) {
        return normalInstructionVisitor.visit(ctx);
    }

    @Override
    public AcolaInstruction visitBooleanInstruction(KellRParser.BooleanInstructionContext ctx) {
        return booleanInstructionVisitor.visit(ctx);
    }
}
