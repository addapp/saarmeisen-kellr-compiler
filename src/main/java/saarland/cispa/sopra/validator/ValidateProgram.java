package saarland.cispa.sopra.validator;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import saarland.cispa.sopra.Brain;
import saarland.cispa.sopra.antlr.KellRLexer;
import saarland.cispa.sopra.antlr.KellRParser;

public class ValidateProgram {
    public Brain kellRParser(String brain) {
        CharStream charStream = CharStreams.fromString(brain);

        KellRLexer lexer = new KellRLexer(charStream);
        lexer.removeErrorListeners();
        DoNotRecoverErrorListener listener = new DoNotRecoverErrorListener();
        lexer.addErrorListener(listener);
        CommonTokenStream stream = new CommonTokenStream(lexer);

        KellRParser parser = new KellRParser(stream);
        parser.removeErrorListeners();
        parser.addErrorListener(listener);

        KellRProgramVisitor kellRProgramVisitor = new KellRProgramVisitor();
        return kellRProgramVisitor.visit(parser.kellR());
    }

    //todo better error messages
    private static class DoNotRecoverErrorListener extends BaseErrorListener {
        @Override
        public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException exception) {
            throw new IllegalArgumentException("In Position " + line + " : " + charPositionInLine + " flew a Syntax error. \"" + offendingSymbol + "\" is wrong because " + msg);
        }

    }
}
