package saarland.cispa.sopra.validator;

import saarland.cispa.sopra.KellREnvironment;
import saarland.cispa.sopra.LabelFactory;
import saarland.cispa.sopra.antlr.KellRBaseVisitor;
import saarland.cispa.sopra.antlr.KellRParser;
import saarland.cispa.sopra.statements.BlockStatement;
import saarland.cispa.sopra.statements.Statement;

import java.util.ArrayList;
import java.util.List;

public class BlockVisitor extends KellRBaseVisitor<BlockStatement> {

    private final LabelFactory labelFactory;
    private final KellREnvironment environment;

    public BlockVisitor(LabelFactory labelFactory, KellREnvironment environment) {
        this.labelFactory = labelFactory;
        this.environment = environment;
    }

    @Override
    public BlockStatement visitBlock(KellRParser.BlockContext ctx) {
        List<Statement> statements = new ArrayList<>(ctx.statementBase().size());
        StatementVisitor statementVisitor = new StatementVisitor(labelFactory, environment);

        for (KellRParser.StatementBaseContext statementBaseContext : ctx.statementBase()) {
            statements.add(statementVisitor.visit(statementBaseContext));
        }

        return new BlockStatement(statements, environment);
    }
}
