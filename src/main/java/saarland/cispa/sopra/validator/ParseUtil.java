package saarland.cispa.sopra.validator;

import org.antlr.v4.runtime.tree.TerminalNode;

public final class ParseUtil {
    private ParseUtil() {}

    public static int parseInt (TerminalNode node) {
        try {
            return Integer.parseInt(node.getText());
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Number format exception while parsing. Probably too big.", e);
        }
    }
}
