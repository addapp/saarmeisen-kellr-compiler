package saarland.cispa.sopra.validator;

import saarland.cispa.sopra.KellRFunction;
import saarland.cispa.sopra.antlr.KellRBaseVisitor;
import saarland.cispa.sopra.antlr.KellRParser;

public class KellRFunctionVisitor extends KellRBaseVisitor<KellRFunction> {

    @Override
    public KellRFunction visitFunctionDeclaration(KellRParser.FunctionDeclarationContext ctx) {
        String name = ctx.IDENTIFIER().getText();
        KellRParser.BlockContext block = ctx.block();

        return new KellRFunction(name, block);
    }
}
