package saarland.cispa.sopra.validator;

import saarland.cispa.sopra.Label;
import saarland.cispa.sopra.LabelFactory;
import saarland.cispa.sopra.antlr.KellRBaseVisitor;
import saarland.cispa.sopra.antlr.KellRParser;
import saarland.cispa.sopra.instructions.AcolaInstructionWithOptions;
import saarland.cispa.sopra.instructions.BreedInstruction;
import saarland.cispa.sopra.instructions.DirectionInstruction;
import saarland.cispa.sopra.instructions.DropInstruction;
import saarland.cispa.sopra.instructions.FlipInstruction;
import saarland.cispa.sopra.instructions.MoveInstruction;
import saarland.cispa.sopra.instructions.PickupInstruction;
import saarland.cispa.sopra.instructions.SenseInstruction;
import saarland.cispa.sopra.instructions.SenseMarkInstruction;
import saarland.cispa.sopra.instructions.TestInstruction;

import static saarland.cispa.sopra.validator.ParseUtil.parseInt;

public class BooleanInstructionVisitor extends KellRBaseVisitor<AcolaInstructionWithOptions> {

    private final LabelFactory labelFactory;
    private Label jumpTo;

    public BooleanInstructionVisitor(LabelFactory labelFactory) {
        this.labelFactory = labelFactory;
    }

    @Override
    public AcolaInstructionWithOptions visitBooleanInstruction(KellRParser.BooleanInstructionContext ctx) {
        jumpTo = null;

        if (ctx.jumpTarget() != null) {
            jumpTo = labelFactory.getLabel(ctx.jumpTarget().LABEL().getText());
        }

        // visit the actual instruction
        return visit(ctx.children.get(0));
    }

    @Override
    public AcolaInstructionWithOptions visitSenseMark(KellRParser.SenseMarkContext ctx) {
        String relativePosition = ctx.relativeposition().getText();
        int index = parseInt(ctx.NUMBER());

        return new SenseMarkInstruction(relativePosition, index, jumpTo);
    }

    @Override
    public AcolaInstructionWithOptions visitTest(KellRParser.TestContext ctx) {
        int index = parseInt(ctx.NUMBER());

        return new TestInstruction(index, jumpTo);
    }

    @Override
    public AcolaInstructionWithOptions visitFlip(KellRParser.FlipContext ctx) {
        int bound = parseInt(ctx.NUMBER());

        return new FlipInstruction(bound, jumpTo);
    }

    @Override
    public AcolaInstructionWithOptions visitDirection(KellRParser.DirectionContext ctx) {
        String direction = ctx.DIRECTION().getText();

        return new DirectionInstruction(direction, jumpTo);
    }

    @Override
    public AcolaInstructionWithOptions visitMove(KellRParser.MoveContext ctx) {
        return new MoveInstruction(jumpTo);
    }

    @Override
    public AcolaInstructionWithOptions visitBreed(KellRParser.BreedContext ctx) {
        return new BreedInstruction(jumpTo);
    }

    @Override
    public AcolaInstructionWithOptions visitSense(KellRParser.SenseContext ctx) {
        String relativePosition = ctx.relativeposition().getText();
        String condition = ctx.CONDITION().getText();

        return new SenseInstruction(relativePosition, condition, jumpTo);
    }

    @Override
    public AcolaInstructionWithOptions visitDrop(KellRParser.DropContext ctx) {
        return new DropInstruction(jumpTo);
    }

    @Override
    public AcolaInstructionWithOptions visitPickup(KellRParser.PickupContext ctx) {
        return new PickupInstruction(jumpTo);
    }
}
