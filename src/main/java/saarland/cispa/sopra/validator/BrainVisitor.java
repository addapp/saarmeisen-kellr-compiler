package saarland.cispa.sopra.validator;

import saarland.cispa.sopra.Brain;
import saarland.cispa.sopra.KellREnvironment;
import saarland.cispa.sopra.LabelFactory;
import saarland.cispa.sopra.antlr.KellRBaseVisitor;
import saarland.cispa.sopra.antlr.KellRParser;
import saarland.cispa.sopra.instructions.JumpInstruction;
import saarland.cispa.sopra.statements.BlockStatement;
import saarland.cispa.sopra.statements.InstructionStatement;
import saarland.cispa.sopra.statements.Statement;

import java.util.List;

public class BrainVisitor extends KellRBaseVisitor<Brain> {
    private final LabelFactory labelFactory;
    private final KellREnvironment environment;

    public BrainVisitor(LabelFactory labelFactory, KellREnvironment environment) {
        this.labelFactory = labelFactory;
        this.environment = environment;
    }

    @Override
    public Brain visitBrain(KellRParser.BrainContext ctx) {
        BlockVisitor blockVisitor = new BlockVisitor(labelFactory, environment);
        BlockStatement block = blockVisitor.visit(ctx.block());

        List<Statement> statements = block.getStatements();
        Statement lastStatement = statements.get(statements.size()-1);
        if (!(lastStatement instanceof InstructionStatement)
            || !(((InstructionStatement)lastStatement).getInstruction() instanceof JumpInstruction)) {
            throw new IllegalArgumentException("The last instruction has to be a Jump Instruction.");
        }

        String name = ctx.IDENTIFIER().getText();
        return new Brain(name, block);
    }
}
