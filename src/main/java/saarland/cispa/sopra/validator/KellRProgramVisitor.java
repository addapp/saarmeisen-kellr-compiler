package saarland.cispa.sopra.validator;

import saarland.cispa.sopra.Brain;
import saarland.cispa.sopra.KellREnvironment;
import saarland.cispa.sopra.LabelFactory;
import saarland.cispa.sopra.antlr.KellRBaseVisitor;
import saarland.cispa.sopra.antlr.KellRParser;

public class KellRProgramVisitor extends KellRBaseVisitor<Brain> {
    @Override
    public Brain visitKellR(KellRParser.KellRContext ctx) {
        LabelFactory labelFactory = new LabelFactory();
        KellREnvironment environment = new KellREnvironment();
        KellRFunctionVisitor functionVisitor = new KellRFunctionVisitor();

        for (KellRParser.FunctionDeclarationContext functionDeclarationContext : ctx.functionDeclaration()) {
            environment.putFunction(functionVisitor.visit(functionDeclarationContext));
        }

        BrainVisitor brainVisitor = new BrainVisitor(labelFactory, environment);
        return brainVisitor.visit(ctx.brain());
    }
}
