package saarland.cispa.sopra.validator;

import saarland.cispa.sopra.KellREnvironment;
import saarland.cispa.sopra.KellRFunction;
import saarland.cispa.sopra.Label;
import saarland.cispa.sopra.LabelFactory;
import saarland.cispa.sopra.antlr.KellRBaseVisitor;
import saarland.cispa.sopra.antlr.KellRParser;
import saarland.cispa.sopra.instructions.AcolaInstruction;
import saarland.cispa.sopra.instructions.AcolaInstructionWithOptions;
import saarland.cispa.sopra.instructions.JumpInstruction;
import saarland.cispa.sopra.statements.BlockStatement;
import saarland.cispa.sopra.statements.FunctionCallStatement;
import saarland.cispa.sopra.statements.IfStatement;
import saarland.cispa.sopra.statements.InstructionStatement;
import saarland.cispa.sopra.statements.Statement;
import saarland.cispa.sopra.statements.WhileStatement;

import java.util.ArrayList;
import java.util.List;

public class StatementVisitor extends KellRBaseVisitor<Statement> {
    private final LabelFactory labelFactory;
    private final List<Label> labels;
    private final KellREnvironment environment;

    public StatementVisitor (LabelFactory labelFactory, KellREnvironment environment) {
        this.labelFactory = labelFactory;
        this.environment = environment;
        labels = new ArrayList<>();
    }

    @Override
    public Statement visitStatementBase(KellRParser.StatementBaseContext ctx) {
        //parse labels and add them to the global labels list
        LabelVisitor labelVisitor = new LabelVisitor(labelFactory);
        if (ctx.labelDefinition() != null) {
            ctx.labelDefinition().forEach(labelContext -> labels.add(labelVisitor.visit(labelContext)));
        }

        Statement statement = visit(ctx.statement());
        labels.clear();
        return statement;
    }

    @Override
    public Statement visitIfStatement(KellRParser.IfStatementContext ctx) {

        boolean negated = ctx.NEGATION() != null;
        BooleanInstructionVisitor conditionVisitor = new BooleanInstructionVisitor(labelFactory);
        AcolaInstructionWithOptions condition = conditionVisitor.visit(ctx.booleanInstructionBase());

        BlockVisitor blockVisitor = new BlockVisitor(labelFactory, environment);
        BlockStatement consequent = blockVisitor.visit(ctx.block(0));
        BlockStatement alternative;
        KellRParser.BlockContext alternativeContext = ctx.block(1);

        if (alternativeContext == null) {
            alternative = null;
        } else {
            alternative = blockVisitor.visit(alternativeContext);
        }

        return new IfStatement(labels, environment, negated, condition, consequent, alternative, labelFactory);
    }

    @Override
    public Statement visitWhileStatement(KellRParser.WhileStatementContext ctx) {
        boolean negated = ctx.NEGATION() != null;
        BooleanInstructionVisitor conditionVisitor = new BooleanInstructionVisitor(labelFactory);
        AcolaInstructionWithOptions condition = conditionVisitor.visit(ctx.booleanInstructionBase());

        BlockVisitor blockVisitor = new BlockVisitor(labelFactory, environment);
        BlockStatement block = blockVisitor.visit(ctx.block());

        return new WhileStatement(labels, environment, negated, condition, block, labelFactory);
    }

    @Override
    public Statement visitFunctionCallStatement(KellRParser.FunctionCallStatementContext ctx) {
        String name = ctx.IDENTIFIER().getText();
        KellRFunction function = environment.getFunction(name);
        KellREnvironment functionEnvironment = new KellREnvironment(environment);

        BlockVisitor blockVisitor = new BlockVisitor(labelFactory, functionEnvironment);
        BlockStatement body = blockVisitor.visit(function.getBody());

        return new FunctionCallStatement(labels, environment, body, labelFactory);
    }

    @Override
    public Statement visitInstruction(KellRParser.InstructionContext ctx) {

        InstructionVisitor instructionVisitor = new InstructionVisitor(labelFactory);
        AcolaInstruction instruction = instructionVisitor.visit(ctx);
        return new InstructionStatement(labels, environment, instruction);
    }

    @Override
    public Statement visitReturnStatement(KellRParser.ReturnStatementContext ctx) {
        //todo hardcoded label name. Kann man das besser machen
        //man koennte immer in jedes environment schon ein return label einfuegen das einfach
        //ueber getUniquieLabel erstellt wurde. das kann man sich dann hier rausziehen
        Label returnLabel = labelFactory.getLabel("$return");
        return new InstructionStatement(labels, environment, new JumpInstruction(returnLabel));
    }
}
