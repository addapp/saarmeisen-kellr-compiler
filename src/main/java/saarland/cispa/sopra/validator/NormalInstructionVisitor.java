package saarland.cispa.sopra.validator;

import saarland.cispa.sopra.Label;
import saarland.cispa.sopra.LabelFactory;
import saarland.cispa.sopra.antlr.KellRBaseVisitor;
import saarland.cispa.sopra.antlr.KellRParser;
import saarland.cispa.sopra.instructions.AcolaInstruction;
import saarland.cispa.sopra.instructions.JumpInstruction;
import saarland.cispa.sopra.instructions.MarkInstruction;
import saarland.cispa.sopra.instructions.SetInstruction;
import saarland.cispa.sopra.instructions.TurnInstruction;
import saarland.cispa.sopra.instructions.UnmarkInstruction;
import saarland.cispa.sopra.instructions.UnsetInstruction;

import static saarland.cispa.sopra.validator.ParseUtil.parseInt;

public class NormalInstructionVisitor extends KellRBaseVisitor<AcolaInstruction> {
    private final LabelFactory labelFactory;

    public NormalInstructionVisitor(LabelFactory labelFactory) {
        this.labelFactory = labelFactory;
    }

    @Override
    public AcolaInstruction visitMark(KellRParser.MarkContext ctx) {
        int markIndex = parseInt(ctx.NUMBER());
        return new MarkInstruction(markIndex);
    }

    @Override
    public AcolaInstruction visitUnmark(KellRParser.UnmarkContext ctx) {
        int markIndex = parseInt(ctx.NUMBER());
        return new UnmarkInstruction(markIndex);
    }

    @Override
    public AcolaInstruction visitSet(KellRParser.SetContext ctx) {
        int setIndex = parseInt(ctx.NUMBER());
        return new SetInstruction(setIndex);
    }

    @Override
    public AcolaInstruction visitUnset(KellRParser.UnsetContext ctx) {
        int setIndex = parseInt(ctx.NUMBER());
        return new UnsetInstruction(setIndex);
    }

    @Override
    public AcolaInstruction visitTurn(KellRParser.TurnContext ctx) {
        String leftRight = ctx.leftright().getText();
        return new TurnInstruction(leftRight);
    }

    @Override
    public AcolaInstruction visitJump(KellRParser.JumpContext ctx) {
        Label jumpTo = labelFactory.getLabel(ctx.jumpTarget().LABEL().getText());
        return new JumpInstruction(jumpTo);
    }
}
