package saarland.cispa.sopra;

import java.util.HashMap;
import java.util.Map;

public class KellREnvironment {
    /**
     * Since labels in functions should only be accessible inside the function itself
     * and since we can have nested function calls, we use this field instead of the more
     * common parent environment so that even in a nested function call we can only
     * access its and the brain's labels.
     */
    private final KellREnvironment brainEnvironment;

    /**
     * A mapping from function names to functions
     */
    private final Map<String, KellRFunction> functions;

    /**
     * A mapping from each label in this environment to its line number
     */
    private final Map<Label, Integer> labels;

    /**
     * The default constructor assumes that the created KellREnvironment is the brainEnvironment
     */
    public KellREnvironment() {
        functions = new HashMap<>();
        labels = new HashMap<>();
        brainEnvironment = null;
    }

    /**
     * This constructor takes another {@link KellREnvironment} and sets brainEnvironment this way
     * @param parent the parent environment
     */
    public KellREnvironment(KellREnvironment parent) {
        functions = new HashMap<>();
        labels = new HashMap<>();
        brainEnvironment = parent.brainEnvironment == null ? parent : parent.brainEnvironment;
    }

    public void putFunction(KellRFunction function) {
        String name = function.getName();
        if (functions.containsKey(name)) {
            throw new IllegalArgumentException(String.format("You are redefining the function: '%s'", name));
        }
        functions.put(name, function);
    }

    public KellRFunction getFunction(String name) {
        KellRFunction function = functions.get(name);

        if (function == null) {
            if (brainEnvironment == null) {
                throw new IllegalArgumentException(String.format("The function '%s' is not defined", name));
            } else {
                return brainEnvironment.getFunction(name);
            }
        } else {
            return function;
        }
    }

    private boolean containsLabel(Label label) {
        if (labels.containsKey(label)) {
            return true;
        } else if (brainEnvironment == null) {
            return false;
        } else {
            return brainEnvironment.containsLabel(label);
        }
    }

    public void putLabel(Label label, int line) {
        if (containsLabel(label)) {
            throw new IllegalArgumentException(String.format("The label %s has already been defined in this scope.", label));
        } else {
            labels.put(label, line);
        }
    }

    public Integer getLabelLine(Label label) {
        Integer line = labels.get(label);
        if (line == null) {
            if (brainEnvironment == null) {
                throw new IllegalArgumentException(String.format("The label %s is not defined in this scope.", label));
            } else {
                return brainEnvironment.getLabelLine(label);
            }
        } else {
            return line;
        }
    }
}
