package saarland.cispa.sopra.instructions;


import saarland.cispa.sopra.Label;

public abstract class AcolaInstructionWithOptions implements AcolaInstruction{

    protected int jumpAddress;
    protected Label jumpTo;

    protected AcolaInstructionWithOptions(Label jumpTo) {
        this.jumpTo = jumpTo;
    }

    @Override
    public String writeToAcola() {
        return String.format("%s else %d%n", getInstructionBase(), jumpAddress);
    }

    protected abstract String getInstructionBase();

    public void setJumpTo(Label jumpTo) {
        this.jumpTo = jumpTo;
    }

    public Label getJumpTo() { return jumpTo; }

    public void setJumpAddress(int jumpAddress) {
        this.jumpAddress = jumpAddress;
    }

    public int getJumpAddress() {
        return jumpAddress;
    }
}
