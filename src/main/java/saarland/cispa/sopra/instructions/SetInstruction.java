package saarland.cispa.sopra.instructions;

import saarland.cispa.sopra.Parameters;


public class SetInstruction implements AcolaInstruction {

    private final int registerindex;

    public SetInstruction(int registerindex) {
        if (registerindex < 0 || registerindex > Parameters.MAX_SET_INDEX) {
            throw new IllegalArgumentException("Invalid register index.");
        }
        this.registerindex = registerindex;
    }


    @Override
    public String writeToAcola() {
        return String.format("set %d%n",registerindex);
    }
}
