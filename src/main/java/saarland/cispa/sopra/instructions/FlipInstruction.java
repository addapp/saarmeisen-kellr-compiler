package saarland.cispa.sopra.instructions;


import saarland.cispa.sopra.Label;

public class FlipInstruction extends AcolaInstructionWithOptions {
    private final int bound;

    public FlipInstruction(int bound, Label jumpTo) {
        super(jumpTo);
        this.bound = bound;
    }


    @Override
    public String getInstructionBase() {
        return "flip " + bound;
    }
}
