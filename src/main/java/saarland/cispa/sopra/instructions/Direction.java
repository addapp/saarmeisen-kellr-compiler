package saarland.cispa.sopra.instructions;

public enum Direction {
    northwest,
    northeast,
    east,
    southeast,
    southwest,
    west;
}
