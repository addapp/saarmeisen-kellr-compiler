package saarland.cispa.sopra.instructions;


public interface AcolaInstruction {
    String writeToAcola();
}
