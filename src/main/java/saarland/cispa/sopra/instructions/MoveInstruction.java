package saarland.cispa.sopra.instructions;


import saarland.cispa.sopra.Label;

public class MoveInstruction extends AcolaInstructionWithOptions {

    public MoveInstruction(Label jumpTo) {
        super(jumpTo);
    }


    @Override
    public String getInstructionBase() {
        return "move";
    }
}
