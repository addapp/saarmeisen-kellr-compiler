package saarland.cispa.sopra.instructions;


import saarland.cispa.sopra.Label;

public class DropInstruction extends AcolaInstructionWithOptions {

    public DropInstruction(Label jumpTo) {
        super(jumpTo);
    }


    @Override
    public String getInstructionBase() {
        return "drop";
    }
}
