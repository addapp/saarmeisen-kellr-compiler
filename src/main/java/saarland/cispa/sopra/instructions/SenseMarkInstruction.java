package saarland.cispa.sopra.instructions;

import saarland.cispa.sopra.Label;
import saarland.cispa.sopra.Parameters;


public class SenseMarkInstruction extends AcolaInstructionWithOptions {
    private final int index;
    private final RelativePosition relativePosition;

    public SenseMarkInstruction(String relativePosition, int markerIndex, Label jumpTo) {
        super(jumpTo);
        this.relativePosition = RelativePosition.valueOf(relativePosition);

        if (markerIndex < 0 || markerIndex > Parameters.MAX_MARK_INDEX) {
            throw new IllegalArgumentException("Invalid marker index.");
        }
        this.index = markerIndex;
    }

    @Override
    public String getInstructionBase() {
        return String.format("sense %s marker %d", relativePosition.name(), index);
    }
}
