package saarland.cispa.sopra.instructions;

import saarland.cispa.sopra.Parameters;

public class MarkInstruction implements AcolaInstruction {
    private final int markerIndex;

    public MarkInstruction(int markerIndex) {
        if (markerIndex < 0 || markerIndex > Parameters.MAX_MARK_INDEX) {
            throw new IllegalArgumentException("Invalid marker index.");
        }
        this.markerIndex = markerIndex;
    }


    @Override
    public String writeToAcola() {
        return String.format("mark %d%n", markerIndex);
    }
}
