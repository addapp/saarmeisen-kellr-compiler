package saarland.cispa.sopra.instructions;

import saarland.cispa.sopra.Parameters;


public class UnmarkInstruction implements AcolaInstruction {
    private final int markerIndex;

    public UnmarkInstruction(int markerIndex) {
        if (markerIndex < 0 || markerIndex > Parameters.MAX_MARK_INDEX) {
            throw new IllegalArgumentException("Invalid marker index.");
        }
        this.markerIndex = markerIndex;
    }


    @Override
    public String writeToAcola() {
        return String.format("unmark %d%n", markerIndex);

    }
}
