package saarland.cispa.sopra.instructions;

import saarland.cispa.sopra.Label;

public class PickupInstruction extends AcolaInstructionWithOptions {

    public PickupInstruction(Label jumpTo) {
        super(jumpTo);
    }


    @Override
    public String getInstructionBase() {
        return "pickup";
    }
}
