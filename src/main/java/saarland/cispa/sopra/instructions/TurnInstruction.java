package saarland.cispa.sopra.instructions;


public class TurnInstruction implements AcolaInstruction {

    private final LeftRight leftright;



    public TurnInstruction(String direction) {
        leftright = LeftRight.valueOf(direction);
    }

    @Override
    public String writeToAcola() {
        return String.format("turn %s%n", leftright.name());
    }
}
