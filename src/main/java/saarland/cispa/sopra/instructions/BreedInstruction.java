package saarland.cispa.sopra.instructions;

import saarland.cispa.sopra.Label;

public class BreedInstruction extends AcolaInstructionWithOptions {

    public BreedInstruction(Label jumpTo){
        super(jumpTo);
    }

    @Override
    public String getInstructionBase() {
        return "breed";
    }


}
