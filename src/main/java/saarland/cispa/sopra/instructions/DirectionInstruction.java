package saarland.cispa.sopra.instructions;

import saarland.cispa.sopra.Label;

public class DirectionInstruction extends AcolaInstructionWithOptions {
    private final Direction direction;

    public DirectionInstruction(String direction, Label jumpTo) {
        super(jumpTo);
        this.direction = Direction.valueOf(direction);
    }

    @Override
    public String getInstructionBase() {
        return "direction " + direction.name();
    }
}
