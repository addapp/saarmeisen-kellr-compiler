package saarland.cispa.sopra.instructions;

public enum Condition {
    friend,
    foe,
    food,
    rock,
    home,
    antlion,
    foemarker,
    foefood,
    friendfood,
    foehome;
}
