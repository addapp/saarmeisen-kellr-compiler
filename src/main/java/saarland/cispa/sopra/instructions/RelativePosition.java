package saarland.cispa.sopra.instructions;

public enum RelativePosition {
    left,
    right,
    here,
    ahead;
}
