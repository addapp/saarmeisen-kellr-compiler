package saarland.cispa.sopra.instructions;

import saarland.cispa.sopra.Label;
import saarland.cispa.sopra.Parameters;


public class TestInstruction extends AcolaInstructionWithOptions {

    private final int testindex;

    public TestInstruction(int testIndex, Label jumpTo) {
        super(jumpTo);
        if (testIndex < 0 || testIndex > Parameters.MAX_SET_INDEX) {
            throw new IllegalArgumentException("Invalid register index.");
        }
        this.testindex = testIndex;
    }


    @Override
    public String getInstructionBase() {
        return "test " + testindex;
    }
}
