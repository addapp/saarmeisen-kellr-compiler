package saarland.cispa.sopra.instructions;


import saarland.cispa.sopra.Label;

public class SenseInstruction extends AcolaInstructionWithOptions {

    private final RelativePosition relativePosition;
    private final Condition condition;

    public SenseInstruction(String relativePos, String condition, Label jumpTo) {
        super(jumpTo);
        this.relativePosition = RelativePosition.valueOf(relativePos);
        this.condition = Condition.valueOf(condition);
    }

    @Override
    public String getInstructionBase() {
        return String.format("sense %s %s", relativePosition.name(), condition.name());
    }
}
