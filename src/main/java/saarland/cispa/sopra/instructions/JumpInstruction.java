package saarland.cispa.sopra.instructions;

import saarland.cispa.sopra.Label;

public class JumpInstruction extends AcolaInstructionWithOptions {

    public JumpInstruction(Label jumpTo) {
        super(jumpTo);
    }

    @Override
    public String writeToAcola() {
        return String.format("jump %d%n", jumpAddress);
    }

    @Override
    protected String getInstructionBase() {
        return null;
    }
}
