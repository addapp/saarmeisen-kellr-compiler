package saarland.cispa.sopra;

public class Label {
    private final int id;
    private final String name;

    public Label(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) { return true; }
        if (obj == null || obj.getClass() != getClass()) { return false; }

        Label label = (Label)obj;

        return id == label.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }
}
