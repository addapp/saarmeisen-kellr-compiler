package saarland.cispa.sopra;

import saarland.cispa.sopra.antlr.KellRParser;

public class KellRFunction {
    public final String name;
    private final KellRParser.BlockContext body;

    public KellRFunction(String name, KellRParser.BlockContext body) {
        this.name = name;
        this.body = body;
    }

    public KellRParser.BlockContext getBody () {
        return body;
    }

    public String getName() {
        return name;
    }
}
