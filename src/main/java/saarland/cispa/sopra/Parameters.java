package saarland.cispa.sopra;

import java.util.ArrayList;
import java.util.List;

public final class Parameters {

    public static final List<Character> VALID_SWARMIDS = new ArrayList<Character>(List.of('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'));
    public static final List<String> RESERVED_KEYWORDS = new ArrayList<>(List.of("brain", "else", "mark", "unmark", "set", "unset", "test", "turn", "move", "sense", "marker", "pickup", "drop", "flip", "jump", "direction", "breed"));

    public static final int MAX_BRAIN_LENGTH = 2500;
    public static final int MAX_MARK_INDEX = 6;
    public static final int MAX_SET_INDEX = 5;
    public final static int FIRST_MAP_LINE = 2;
    public final static char POINT = '.';
    public final static char ROCK = '#';
    public final static char ANTLION = '=';
    public final static char FIRST_VALID_SWARMID = 'A';
    public final static char MOVE_REST_TIME = 13;

    private Parameters() {
    }


}
