package saarland.cispa.sopra;

import saarland.cispa.sopra.validator.ValidateProgram;

public class Compiler {
    public String compile(String program) {
        ValidateProgram validateProgram = new ValidateProgram();
        Brain brain = validateProgram.kellRParser(program);
        return brain.writeToAcola();
    }
}
