package saarland.cispa.sopra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;


public final class Main {

    private final static Logger logger = LoggerFactory.getLogger(Main.class);

    private static final String defaultString = "";

    private Main() {

    }

    public static void main(String[] args) {
        logger.info("Hello KI!");
        String input=defaultString;
        String out=defaultString;
        for(int i= 0; i<args.length;i++){
            switch (args[i]){
                case "--ki":{
                    input = getKI(args,i);
                    i++;
                    break;
                }
                case "--out":{
                    out = getOut(args,i);
                    i++;
                    break;
                }
                default:
                    throw new IllegalArgumentException("wrong argument");
            }
        }

        File brainfile = new File(input);
        String brainString;
        try {
            brainString = new String(Files.readAllBytes(Paths.get(brainfile.getPath())), StandardCharsets.UTF_8);
            logger.debug(brainString);
        } catch (IOException e) {
            throw new IllegalArgumentException("could not read brain of the ki", e);
        }


        Compiler compiler = new Compiler();
        String compiledBrain = compiler.compile(brainString);
        logger.debug(compiledBrain);

        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(out));
            writer.write(compiledBrain);
            writer.close();
        } catch (IOException e) {
            throw new IllegalArgumentException("could not open output file", e);
        }
    }


    private static String getKI(String[] args, int num){
        if(args.length>=num+1){
            return args[num+1];
        }else {
            throw new IllegalArgumentException("wrong argument for ki");
        }
    }

    private static String getOut(String[] args, int num){
        if(args.length>=num+1){
            return args[num+1];
        }else {
            throw new IllegalArgumentException("wrong argument for out");
        }
    }

}
