package saarland.cispa.sopra.statements;

import saarland.cispa.sopra.KellREnvironment;
import saarland.cispa.sopra.Label;
import saarland.cispa.sopra.LabelFactory;
import saarland.cispa.sopra.instructions.AcolaInstructionWithOptions;
import saarland.cispa.sopra.instructions.JumpInstruction;

import java.util.ArrayList;
import java.util.List;

public class IfStatement extends Statement {
    private final LabelFactory labelFactory;
    private final AcolaInstructionWithOptions condition;
    private final BlockStatement consequent;
    private final BlockStatement alternative;
    private final boolean negated;

    /**
     * Constructs an if statement. If The condition is negated consequent and alternative are simply swapped.
     *
     * @param labels The list of labels for the statement
     * @param negated If the condition is negated
     * @param condition the condition
     * @param consequent the consequent
     * @param alternative the alternative
     * @param labelFactory a factory for labels used to construct the if statement in acola
     */
    public IfStatement(List<Label> labels, KellREnvironment environment, boolean negated, AcolaInstructionWithOptions condition, BlockStatement consequent, BlockStatement alternative, LabelFactory labelFactory) {
        super(labels, environment);

        this.negated = negated;
        this.condition = condition;

        if (alternative == null || !negated) {
            this.consequent = consequent;
            this.alternative = alternative;
        } else {
            this.consequent = alternative;
            this.alternative = consequent;
        }
        this.labelFactory = labelFactory;
    }

    @Override
    public List<InstructionStatement> compileStatement() {
        List<InstructionStatement> instructions = new ArrayList<>();

        // create new Label that jumps to alternative
        Label endLabel = labelFactory.getUniqueLabel();
        // create new label that jumps to after the consequent
        Label elseLabel = labelFactory.getUniqueLabel();
        instructions.add(new InstructionStatement(labels, environment, condition));

        if (alternative == null) {
            if (negated) {
                Label startLabel = labelFactory.getUniqueLabel();
                condition.setJumpTo(startLabel);
                instructions.add(new InstructionStatement(List.of(), environment, new JumpInstruction(endLabel)));

                consequent.getStatements().get(0).addLabel(startLabel);
            } else {
                condition.setJumpTo(endLabel);
            }

            instructions.addAll(consequent.compileStatement());
        } else {
            // add that to condition to complete the boolean instruction
            condition.setJumpTo(elseLabel);
            instructions.addAll(consequent.compileStatement());

            // add new jump instruction with that label after the consequence
            instructions.add(new InstructionStatement(List.of(), environment, new JumpInstruction(endLabel)));

            // add elseLabel as a label to the first Instruction in alternative
            List<InstructionStatement> alternativeInstructions = alternative.compileStatement();
            alternativeInstructions.get(0).addLabel(elseLabel);
            instructions.addAll(alternativeInstructions);
        }

        // add a NoopInstruction with the endLabel as the last Instruction
        instructions.add(new NoopStatement(List.of(endLabel), environment));

        return instructions;

    }
}
