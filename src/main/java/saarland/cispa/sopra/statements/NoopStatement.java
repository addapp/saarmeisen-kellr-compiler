package saarland.cispa.sopra.statements;

import saarland.cispa.sopra.KellREnvironment;
import saarland.cispa.sopra.Label;

import java.util.List;

/**
 * noop statement that is just used as a label holder for things like if-statements (maybe later while statements)
 * When the labels are assigned their line numbers. The noop statement shall not increase the line value.
 */
public class NoopStatement extends InstructionStatement {
    public NoopStatement(List<Label> labels, KellREnvironment environment) {
        super(labels, environment, null);
    }

    @Override
    public void writeToAcola(StringBuilder builder) {
        //noop statement does nothing
    }

    @Override
    public List<InstructionStatement> compileStatement() {
        throw new IllegalStateException("This should never be called.");
    }

    @Override
    public int setLabelsLine(int line) {
        assignLabels(line);
        return line;
    }

    @Override
    public boolean isProperInstruction () { return false; }
}
