package saarland.cispa.sopra.statements;

import saarland.cispa.sopra.KellREnvironment;
import saarland.cispa.sopra.Label;
import saarland.cispa.sopra.LabelFactory;

import java.util.List;

public class FunctionCallStatement extends Statement {
    private final BlockStatement body;
    private final LabelFactory labelFactory;

    public FunctionCallStatement(List<Label> labels, KellREnvironment environment, BlockStatement body, LabelFactory labelFactory) {
        super(labels, environment);
        this.body = body;
        this.labelFactory = labelFactory;
    }

    @Override
    public List<InstructionStatement> compileStatement() {
        List<InstructionStatement> bodyInstructions = body.compileStatement();
        bodyInstructions.add(0, new NoopStatement(labels, environment));

        Label returnLabel = labelFactory.getLabel("$return");
        bodyInstructions.add(new NoopStatement(List.of(returnLabel), body.getEnvironment()));
        return bodyInstructions;
    }
}
