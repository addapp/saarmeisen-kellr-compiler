package saarland.cispa.sopra.statements;

import saarland.cispa.sopra.KellREnvironment;

import java.util.ArrayList;
import java.util.List;

public class BlockStatement extends Statement {
    private final List<Statement> statements;

    public BlockStatement(List<Statement> statements, KellREnvironment environment) {
        super(List.of(), environment);
        this.statements = statements;
    }

    @Override
    public List<InstructionStatement> compileStatement() {
        List<InstructionStatement> instructions = new ArrayList<>();
        statements.forEach(statement -> instructions.addAll(statement.compileStatement()));
        return instructions;
    }

    public List<Statement> getStatements() {
        return statements;
    }

    public KellREnvironment getEnvironment() {
        return environment;
    }
}
