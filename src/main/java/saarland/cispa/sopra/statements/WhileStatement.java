package saarland.cispa.sopra.statements;

import saarland.cispa.sopra.KellREnvironment;
import saarland.cispa.sopra.Label;
import saarland.cispa.sopra.LabelFactory;
import saarland.cispa.sopra.instructions.AcolaInstructionWithOptions;
import saarland.cispa.sopra.instructions.JumpInstruction;

import java.util.ArrayList;
import java.util.List;

public class WhileStatement extends Statement {
    private final boolean negated;
    private final LabelFactory labelFactory;
    private final AcolaInstructionWithOptions condition;
    private final BlockStatement block;

    public WhileStatement(List<Label> labels, KellREnvironment environment, boolean negated, AcolaInstructionWithOptions condition, BlockStatement block, LabelFactory labelFactory) {
        super(labels, environment);
        this.negated = negated;
        this.labelFactory = labelFactory;
        this.condition = condition;
        this.block = block;
    }

    @Override
    public List<InstructionStatement> compileStatement() {
        List<InstructionStatement> instructions = new ArrayList<>();

        Label conditionLabel = labelFactory.getUniqueLabel();
        Label endLabel = labelFactory.getUniqueLabel();
        InstructionStatement conditionInstruction = new InstructionStatement(labels, environment, condition);
        conditionInstruction.addLabel(conditionLabel);
        instructions.add(conditionInstruction);

        if (negated) {
            Label startLabel = labelFactory.getUniqueLabel();
            condition.setJumpTo(startLabel);
            instructions.add(new InstructionStatement(List.of(), environment, new JumpInstruction(endLabel)));

            block.getStatements().get(0).addLabel(startLabel);
        } else {
            condition.setJumpTo(endLabel);
        }

        instructions.addAll(block.compileStatement());
        instructions.add(new InstructionStatement(List.of(), environment, new JumpInstruction(conditionLabel)));
        instructions.add(new NoopStatement(List.of(endLabel), environment));

        return instructions;
    }
}
