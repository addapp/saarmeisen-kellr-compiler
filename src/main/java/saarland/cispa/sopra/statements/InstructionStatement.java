package saarland.cispa.sopra.statements;

import saarland.cispa.sopra.KellREnvironment;
import saarland.cispa.sopra.Label;
import saarland.cispa.sopra.instructions.AcolaInstruction;
import saarland.cispa.sopra.instructions.AcolaInstructionWithOptions;
import saarland.cispa.sopra.instructions.JumpInstruction;

import java.util.List;

public class InstructionStatement extends Statement {

    private final AcolaInstruction instruction;
    private boolean squashed;

    public InstructionStatement(List<Label> labels, KellREnvironment environment, AcolaInstruction instruction) {
        super(labels, environment);
        this.instruction = instruction;
    }

    public AcolaInstruction getInstruction() {
        return instruction;
    }

    @Override
    public List<InstructionStatement> compileStatement() {
        return List.of(this);
    }

    public void writeToAcola(StringBuilder builder) {
        builder.append(instruction.writeToAcola());
    }

    protected void assignLabels(int line) {
        labels.forEach(label -> environment.putLabel(label, line));
    }

    public int setLabelsLine(int line) {
        assignLabels(line);
        return line + 1;
    }

    public void assignJumpAddress() {
        if (instruction instanceof  AcolaInstructionWithOptions) {
            AcolaInstructionWithOptions instructionWithOptions = (AcolaInstructionWithOptions)instruction;
            instructionWithOptions.setJumpAddress(environment.getLabelLine(instructionWithOptions.getJumpTo()));
        }
    }

    public boolean isJump() {
        return instruction instanceof JumpInstruction;
    }

    public boolean isJumpStart() {
        return instruction instanceof AcolaInstructionWithOptions;
    }

    public int getJumpAddress() {
        return ((AcolaInstructionWithOptions)instruction).getJumpAddress();
    }

    public void setJumpAddressAndSquash(int jumpAdress) {
        ((AcolaInstructionWithOptions)instruction).setJumpAddress(jumpAdress);
        squashed = true;
    }

    public boolean isProperInstruction () { return true; }

    public boolean isSquashed() {
        return squashed;
    }
}
