package saarland.cispa.sopra.statements;

import saarland.cispa.sopra.KellREnvironment;
import saarland.cispa.sopra.Label;

import java.util.ArrayList;
import java.util.List;

public abstract class Statement {
    protected final List<Label> labels;
    protected final KellREnvironment environment;

    protected Statement(List<Label> labels, KellREnvironment environment) {
        this.labels = new ArrayList<>(labels);
        this.environment = environment;
    }

    public abstract List<InstructionStatement> compileStatement();

    public final void addLabel(Label newLabel) {
        labels.add(newLabel);
    }
}
