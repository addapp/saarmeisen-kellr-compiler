package saarland.cispa.sopra.unittests;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import saarland.cispa.sopra.Compiler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class CompileTest {
    private static final Logger logger = LoggerFactory.getLogger(CompileTest.class);

    @TestFactory
    public Stream<DynamicTest> compileTest () {

        Compiler compiler = new Compiler();
        File kellRBrainDirectory = new File("src/test/resources/KellRBrains/");

        File[] kellRBrains = kellRBrainDirectory.listFiles();
        Stream<File> tests = Stream.of(kellRBrains);

        return tests.map(kellRBrain -> DynamicTest.dynamicTest(kellRBrain.getName(), () -> {
            try {
                logger.debug(kellRBrain.getName());
                String kellRBrainString = new String(Files.readAllBytes(kellRBrain.toPath()), StandardCharsets.UTF_8);
                String compiledBrain = compiler.compile(kellRBrainString);

                logger.debug(kellRBrainString);
                logger.debug(compiledBrain);

                try {
                    BufferedWriter writer = Files.newBufferedWriter(Paths.get(String.format("src/test/resources/compiledBrains/%s", kellRBrain.getName())));
                    writer.write(compiledBrain);
                    writer.close();
                } catch (IOException e) {
                    throw new IllegalArgumentException("could not open output file", e);
                }

            } catch (IOException e) {
                throw new IllegalArgumentException(String.format("Failed loading file %s", kellRBrain.toString()), e);
            }
            }));
    }
}
