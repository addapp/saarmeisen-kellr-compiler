package saarland.cispa.sopra.unittests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import saarland.cispa.sopra.Compiler;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.stream.Stream;

public class FaultyBrainsTest {
    private static final Logger logger = LoggerFactory.getLogger(FaultyBrainsTest.class);

    @TestFactory
    public Stream<DynamicTest> faultyBrainsTest() {

        Compiler compiler = new Compiler();
        File faultyBrainsDirectory = new File("src/test/resources/faultyBrains");

        File[] faultyBrains = faultyBrainsDirectory.listFiles();
        Stream<File> tests = Stream.of(faultyBrains);

        return tests.map(faultyBrain -> DynamicTest.dynamicTest(faultyBrain.toString(), () -> {
            try {
                String faultyBrainString = new String(Files.readAllBytes(faultyBrain.toPath()), StandardCharsets.UTF_8);
                Throwable t = Assertions.assertThrows(IllegalArgumentException.class, () -> compiler.compile(faultyBrainString));
                logger.debug("Test did throw an exception", t);

            } catch (IOException e) {
                throw new IllegalArgumentException(String.format("Failed loading file %s", faultyBrain.toString()), e);
            }
        }));
    }
}

